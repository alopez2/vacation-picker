const express = require('express');
const path = require ('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

mongoose.connect('mongodb://localhost:27017/vacation-picker', 
{ 
    promiseLibrary: require('bluebird'),
    useNewUrlParser: true
})
.then(() => console.log('mongo connection successful'))
.catch((err) => console.error(err));

const locations = require('./routes/locations');
const auth = require('./routes/auth');
const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': 'false'}));
app.use(express.static(path.join(__dirname, 'build')));

app.use('/api/locations', locations);
app.use('/api/auth', auth);

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.send('error');
});

module.exports = app;