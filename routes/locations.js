const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Locations = require('../models/locations.js');
const passport = require('passport');
require('../config/passport')(passport);

/* GET ALL LOCATIONS */
router.get('/allLocations', passport.authenticate('jwt', { session: false}), (req, res, next) => {
    var token = getToken(req.headers);
    if (token) {
        Locations.find((err, locations) => {
            if (err) {
                return next(err);
            }
            res.json(locations);
        });
    } else {
        return res.status(403).send({
            success: false,
            msg: 'Unauthorized'
        });
    }
});

/* SAVE LOCATION */
router.post('/', passport.authenticate('jwt', { session: false}), (req, res, next) => {
    var token = getToken(req.headers);
    if (token) {
        Locations.create(req.body, (err, post) => {
            if (err) {
                return next(err);
            }
            res.json(post);
        });
    } else {
        return res.status(403).send({
            success: false,
            msg: 'Unauthorized'
        });
    }
});

router.get('/', (req, res, next) => {
    res.send('Express rest API GET Locations');
});

getToken = (headers) => {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

module.exports = router;