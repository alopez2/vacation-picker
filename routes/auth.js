const mongoose = require('mongoose');
const passport = require('passport');
const settings = require('../config/settings');
require('../config/passport')(passport);
const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('../models/user');

router.post('/register', (req, res) => {
    if (!req.body.username || !req.body.password) {
        res.json({
            success: false,
            msg: 'Username and Password required'
        });
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password
        });

        newUser.save((err) => {
            if (err) {
                return res.json({
                    success: false,
                    msg: 'Username already exists'
                });
            }
            res.json({
                success: true,
                msg: 'Successfully created new user'
            });
        });
    }
});

router.post('/login', (req, res) => {
    User.findOne({
        username: req.body.username
    }, (err, user) => {
        if (err) {
            throw err;
        }
        if (!user) {
            res.status(401).send({
                success: false,
                msg: 'Authentication failed'
            });
        } else {
            user.comparePassword(req.body.password, (err, isMatch) => {
                if (isMatch && !err) {
                    var token = jwt.sign(user.toJSON(), settings.secret);
                    res.json({
                        success: true,
                        token: `JWT ${token}`
                    });
                } else {
                    res.status(401).send({
                        success: false,
                        msg: 'Authentication failed'
                    });
                }
            });
        }
    });
});

module.exports = router;