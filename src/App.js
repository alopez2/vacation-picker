import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FlightTakeoff from '@material-ui/icons/FlightTakeoff';
import Fab from '@material-ui/core/Fab';
import './App.css';

const lengths = [
  {
    value: 0,
    label: '1-3'
  },
  {
    value: 1,
    label: '4-7'
  },
  {
    value: 2,
    label: '8-11'
  },
  {
    value: 3,
    label: '12-15'
  },
  {
    value: 4,
    label: '16-20'
  }
]

class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      locations: [],
      modalOpen: false,
      location: '',
      length: 0,
      vacation_length: -1
    };
  }

  componentDidMount() {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwtToken');
    axios.get('/api/locations/allLocations')
      .then(res => {
        this.setState({
          locations: res.data
        });
      })
      .catch((error) => {
        if(error.response.status === 401) {
          this.props.history.push('/login');
        }
      });
  }

  getNextVacationPlace = () => {
    const places = [];
    this.state.locations.map(location => {
      console.log(`location: ${parseInt(location.length, 10)}`);
      console.log(`vacation: ${this.state.vacation_length}`);
      if (this.state.vacation_length === -1 || parseInt(location.length, 10) === this.state.vacation_length) {
        console.log('in len');
        places.push(location.location);
      }
    });
    if (places.length === 0 ) {
      alert('No places match')
    } else {
      alert(`We're going to ${places[Math.floor(Math.random() * Math.floor(places.length))]}!`);
    }
  }

  handleModalState = () => {
    //console.log(this.state.modalOpen);
    this.setState({
      modalOpen: !this.state.modalOpen
    });
    //console.log(this.state);
  }

  logout = () => {
    localStorage.removeItem('jwtToken');
    window.location.reload();
  }

  addLocation = (e) => {
    e.preventDefault();
    const { location, length } = this.state;
    const owner = localStorage.getItem('currentUser');
    const d = new Date();
    const date = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()} ${d.getHours() % 12}:${d.getMinutes()}:${d.getSeconds()}.${d.getMilliseconds()}`;
    axios.post('/api/locations/', { location, length, owner, date })
      .then((result) => {
        window.location.reload();
      })
      .catch((error) => {
          if (error.response.status === 403) {
              alert('Failed to save');
          }
      });
    this.handleModalState();
  }

  onChange = (e) => {
    const state = this.state;

    state[e.target.name] = e.target.value;
    this.setState(state);
}

  render() {
   const { classes } = this.props;
   return (
    <div class='container'>
      <div class='panel panel-container'>
        <div class='panel-heading'>
          <h3 class='panel-title'>
            Locations &nbsp;
            {localStorage.getItem('jwtToken') &&
              <div class='buttons'>
                <Button variant='contained' color='primary' onClick={this.handleModalState}>
                  Add Location
                </Button>
                <div class='middle-buttons'>
                  <TextField
                    id='location-visit-length'
                    select
                    label='How Long?'
                    name='vacation_length'
                    style={{marginRight: '10px'}}
                    value={this.state.vacation_length}
                    onChange={this.onChange}
                    margin='normal'
                    variant='outlined'
                    
                    InputProps={{
                      endAdornment: (
                        <InputAdornment variant='filled' position='end'>
                          Days
                        </InputAdornment>
                      )
                    }}
                  >
                    {lengths.map(option => (
                      <MenuItem 
                        key={option.value} 
                        value={option.value}
                      >
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                  <Fab color='secondary' aria-label='Lets Go' onClick={this.getNextVacationPlace}>
                    <FlightTakeoff />
                  </Fab>
                </div>
                <Button variant='contained' onClick={this.logout}>
                  Logout
                </Button>
              </div>
            }
          </h3>
        </div>
        <div class='panel-body'>
            <table class='table table-stripe'>
              <thead>
                <tr>
                  <th>Owner</th>
                  <th>Location</th>
                  <th>Length</th>
                </tr>
              </thead>
              <tbody>
                {this.state.locations.map(location =>
                  <tr>
                    <td>
                      {location.owner}
                    </td>
                    <td>
                      {location.location}
                    </td>
                    <td>
                      {lengths[location.length].label}
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
        </div>
      </div>
      <Modal
        aria-labelledby='add-location-modal'
        aria-describedby='add-location-modal-description'
        open={this.state.modalOpen}
        onClose={this.handleModalState}
      >
        <div class='add-location-modal'>
        <div class='add-location-content'>
            <Typography variant='h6' id='add-location-modal-title'>
              <span>Add Location</span>
            </Typography>
            <form class='form-signin' onSubmit={this.addLocation}>
              <TextField
                id='location-name'
                label='Location'
                name='location'
                margin='normal'
                variant='outlined'
                onChange={this.onChange}
              />
              <TextField
                id='location-visit-length'
                select
                label='Vacation Length'
                name='length'
                value={this.state.length}
                onChange={this.onChange}
                margin='normal'
                variant='outlined'
                style={{width: '100%'}}
                InputProps={{
                  endAdornment: (
                    <InputAdornment variant='filled' position='end'>
                      Days
                    </InputAdornment>
                  )
                }}
              >
                {lengths.map(option => (
                  <MenuItem 
                    key={option.value} 
                    value={option.value}
                  >
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
              <br/>
              <Button variant='contained' color='primary' onClick={this.addLocation}>
                Submit
              </Button>
            </form>
          </div>
        </div>
      </Modal>
    </div>
   );
  }
}

// App.propTypes = {
//   classes: PropTypes.object.isRequired
// };

// const SimpleModalWrapped = withStyles(styles)(App);

export default App;