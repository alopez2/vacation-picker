const mongoose = require('mongoose');

const LocationSchema = new mongoose.Schema({
    location: String,
    date: { type: Date },
    owner: String,
    length: String
});

module.exports = mongoose.model('locations', LocationSchema);