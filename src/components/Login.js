import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './Login.css';

class Login extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            message: ''
        };
    }

    onChange = (e) => {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();

        const { username, password } = this.state;

        axios.post('/api/auth/login', { username, password })
            .then((result) => {
                localStorage.setItem('jwtToken', result.data.token);
                localStorage.setItem('currentUser', username);
                this.setState({
                    message: ''
                });
                this.props.history.push('/');
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    this.setState({
                        message: 'Login Failed'
                    });
                }
            });
    }

    render() {
        const { username, password, message} = this.state;
        return (
            <div class='container'>
                <form class='form-signin' onSubmit={this.onSubmit}>
                    {message !== '' &&
                        <div class='alert alert-warning alert-dismissable' role='alert'>
                            { message }
                        </div>
                    }
                    <h2 class='form-signin-heading'>
                        Please Sign In
                    </h2>
                    <label for='inputEmail' class='sr-only'>Email</label>
                    <input type='email' class='form-control' placeholder='Email Address' name='username' value={username} onChange={this.onChange} required />
                    <label for='inputPassword' class='sr-only'>Password</label>
                    <input type='password' class='form-control' placeholder='Password' name='password' value={password} onChange={this.onChange} required />
                    <button class='btn btn-lg btn-primary' type='submit'>Login</button>
                    <p>
                        Not signed up yet? 
                        <Link to='/register'>
                            <span class='glyphicon glyphicon-plus-sign' aria-hidden='true'>
                            </span> Register Here
                        </Link>
                    </p>
                </form>
            </div>
        );
    }
}

export default Login;